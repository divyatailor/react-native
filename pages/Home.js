import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, ActivityIndicator, Image, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { Card } from 'react-native-elements';
import axios from 'axios';

export default class Home extends React.Component {
  state = {
    data: [],
    loading: true,
    error: false,
    isRefreshing: false,
    page: 1
  }

  componentDidMount() {
    let token = JSON.parse(localStorage.getItem('token'));
    let header = {
      'Authorization': token.type + " " + token.token
    }
    axios.get('http://68.183.48.101:3333/users/list?page=' + this.state.page, { headers: header })
      .then((response) => {
        console.log(response.data.data.users);
        this.setState({
          data: response.data.data.users,
          loading: false
        })
      }, err => {
        console.log(err.message);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }
  onRefresh = () => {
    let token = JSON.parse(localStorage.getItem('token'));
    let header = {
      'Authorization': token.type + " " + token.token
    }
    this.state.page = this.state.page + 1;
    axios.get('http://68.183.48.101:3333/users/list?page=' + this.state.page, { headers: header })
      .then((response) => {
        console.log(response.data.data.users);
        this.setState({
          data: response.data.data.users,
          loading: false,
          isRefreshing: false
        })
      }, err => {
        console.log(err.message);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };
  render() {
    const { data, loading, error, search } = this.state;
    return (
      <View>
        <ScrollView refreshControl={
          <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={this.onRefresh}
          />
        }>
          {loading && <ActivityIndicator size="small" color="#00ff00" />}
          {!loading && !error &&
            this.state.data.map((data, i) => (
              <Card containerStyle={{
                borderRadius: 12, height: 86, justifyContent: 'center'
              }} key={data['id']}>
                <View style={{ flexDirection: 'row' }} >
                  <Image source={{ uri: data['profile_pic'] }} style={{ width: 100, height: 70 }} />
                  <View style={{ paddingLeft: 10, justifyContent: 'center' }}>
                    <Text numberOfLines={1} style={styles.title}>{data['username']}</Text>
                    <Text style={styles.brand}>{data['email']}</Text>
                  </View>
                </View>
              </Card>
            ))
          }
          {error && <div>Error message</div>}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '38%',
    // borderRadius: 8
    borderBottomRightRadius: 280,
    borderBottomLeftRadius: 280,
    position: 'absolute',
    width: '140%',
    marginLeft: -78,
    // marginTop: -101
  },
  text: {
    textAlign: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    color: '#fefffe',
    fontSize: 18,
    // fontFamily: fonts.medium
  },
  title: {
    fontSize: 15,
    width: 220,
    // flexWrap:'wrap',
    fontWeight: 'bold',
    lineHeight: 19,
    color: '#626262',
    fontFamily: 'SFCompactDisplay',
    paddingBottom: 10
  },
  brand: {
    fontSize: 15,
    lineHeight: 18,
    fontFamily: 'SFCompactDisplay',
    color: 'darkgray'
  },
});
