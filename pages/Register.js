import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, TextInput, TouchableHighlight } from 'react-native';
import axios from 'axios';

export default class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      input: {},
      errors: {}
    }
  }

  handleChange = (ev, name) => {
    ev.persist();
    let input = this.state.input;
    input[name] = ev.target.value;
    this.setState({
      input
    });
  }

  register = () => {
    if (this.validate()) {
      let input = {};
      input["username"] = "";
      input["email"] = "";
      input["password"] = "";
      input["confirm_password"] = "";
      this.setState({ input: input });
      let postData = {
        "username": this.state.input.username,
        "email": this.state.input.email,
        "password": this.state.input.password
      }
      axios.post('http://68.183.48.101:3333/users/register', postData)
        .then((response) => {
          console.log(response);
          let data = response;
          console.log(data['meta']);
          localStorage.setItem('userData', JSON.stringify(data.data['data']['user']));
          localStorage.setItem('token', JSON.stringify(data.data['data']['token']));
          alert(data.data.meta.message);
          this.props.navigation.navigate('Home');
        })
        .catch((error) => {
          alert(error.response.data.meta.message);
        });
    }
  }

  validate() {
    let input = this.state.input;
    let errors = {};
    let isValid = true;
    if (!input["username"]) {
      isValid = false;
      errors["username"] = "Please enter your username.";
    }
    if (typeof input["username"] !== "undefined") {
      const re = /^\S*$/;
      if (input["username"].length < 6 || !re.test(input["username"])) {
        isValid = false;
        errors["username"] = "Please enter only character.";
      }
    }
    if (!input["email"]) {
      isValid = false;
      errors["email"] = "Please enter your email Address.";
    }
    if (typeof input["email"] !== "undefined") {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(input["email"])) {
        isValid = false;
        errors["email"] = "Please enter valid email address.";
      }
    }
    if (!input["password"]) {
      isValid = false;
      errors["password"] = "Please enter your password.";
    }
    if (!input["confirm_password"]) {
      isValid = false;
      errors["confirm_password"] = "Please enter your confirm password.";
    }
    if (typeof input["password"] !== "undefined") {
      if (input["password"].length < 6) {
        isValid = false;
        errors["password"] = "Please add at least 6 charachter.";
      }
    }
    if (typeof input["password"] !== "undefined" && typeof input["confirm_password"] !== "undefined") {
      if (input["password"] != input["confirm_password"]) {
        isValid = false;
        errors["password"] = "Passwords don't match.";
      }
    }
    this.setState({
      errors: errors
    });
    return isValid;
  }

  render() {
    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <Text style={styles.text}>Registration</Text>
          <TextInput
            style={styles.input}
            name="username"
            id="username"
            value={this.state.input.username}
            onChange={(event) => this.handleChange(event, "username")}
            placeholder="Enter Username"
            autoCompleteType="username"
          />
          <Text style={styles.errorText}>{this.state.errors.username}</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter Email"
            name="email"
            value={this.state.input.email}
            onChange={(event) => this.handleChange(event, "email")}
            keyboardType="email-address"
            autoCompleteType="email"
          />
          <Text style={styles.errorText}>{this.state.errors.email}</Text>
          <TextInput
            style={styles.input}
            name="password"
            value={this.state.input.password}
            onChange={(event) => this.handleChange(event, "password")}
            placeholder="Enter Password"
            autoCompleteType="password"
          />
          <Text style={styles.errorText}>{this.state.errors.password}</Text>
          <TextInput
            style={styles.input}
            name="confirm_password"
            value={this.state.input.confirm_password}
            onChange={(event) => this.handleChange(event, "confirm_password")}
            placeholder="Enter Confirm Password"
            autoCompleteType="password"
          />
          <Text style={styles.errorText}>{this.state.errors.confirm_password}</Text>
          <TouchableHighlight
            style={styles.button}>
            <Button
              title="Register"
              onPress={this.register}
            />
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    justifyContent: 'center',
    // justifyContent: "flex-start",
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    padding: 50,

  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    padding: 20,
    color: "#000000"
  },
  button: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 50
  },
  errorText: {
    color: "red",
    fontWeight: "bold",
    fontSize: 12,
    paddingLeft: 20,
    marginTop: -11,
    paddingBottom: 10
  }
});
